<?php

namespace Database\Seeders;
use App\Models\Editorial;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
            Editorial::create(
            [
                'id' => 1,
                'nombre' => 'Prueba Editorial 1'
            ]);
            Editorial::create([
                'id' => 2,
                'nombre' => 'Prueba Editorial 2'
            ]);
            Editorial::create([
                'id' => 3,
                'nombre' => 'Prueba Editorial 3'
            ]);
            Editorial::create([
                'id' => 4,
                'nombre' => 'Prueba Editorial 4'
            ]);
            Editorial::create([
                'id' => 5,
                'nombre' => 'Prueba Editorial 5'
            ]);
            Editorial::create(
                [
                    'id' => 6,
                    'nombre' => 'Prueba Editorial 6'
                ]);
            Editorial::create([
                    'id' => 7,
                    'nombre' => 'Prueba Editorial 7'
                ]);
            Editorial::create([
                    'id' => 8,
                    'nombre' => 'Prueba Editorial 8'
                ]);
            Editorial::create([
                    'id' => 9,
                    'nombre' => 'Prueba Editorial 9'
                ]);
            Editorial::create([
                    'id' => 10,
                    'nombre' => 'Prueba Editorial 10'
                ]);
    }
}
