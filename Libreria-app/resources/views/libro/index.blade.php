@extends('layouts.app')

@section('content')
<h1 class="text-3xl text-center text-blue-500 mt-4">Listado de clientes</h1>
<div class="flex flex-col">
    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
        <div class="overflow-hidden">
          <table class="min-w-full text-center text-sm font-light">
            <thead class="border-b font-medium dark:border-neutral-500">
            <tr class="bg-blue-500 text-xl text-white">
                <th scope="col" class="px-6 py-4">Titulo</th>
                <th scope="col" class="px-6 py-4">Editorial</th>
                <th scope="col" class="px-6 py-4">Edicion</th>    
                <th scope="col" class="px-6 py-4">Pais</th>
                <th scope="col" class="px-6 py-4">Precio</th>
                <th scope="col" class="px-6 py-4">Acciones</th>

              </tr>
            </thead>
            <tbody>
                @foreach ($libros as $libro)
                <tr class="border-b dark:border-neutral-500">
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->titulo}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->editorial->nombre}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->edicion}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->pais}}</td>
                    <td class="whitespace-nowrap px-6 py-4">{{$libro->precio}}</td>
                    <td class="whitespace-nowrap px-6 py-4">
                        <a href="{{route('libro.show',$libro->id)}}"class="bg-blue-500 text-white px-2 py-1 rounded-md mr-2">Mostrar</a>
                        <a href="{{route('libro.edit',$libro->id)}}"class="bg-yellow-500 text-white px-2 py-1 rounded-md mr-2">Editar</a>
                    
                        <form action="{{route('libro.destroy',$libro->id)}}" method="post" class="inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit"class="bg-red-500 text-white px-2 py-1 rounded-md mr-2">Eliminar</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


<a href="{{route('libro.create')}}"class="bg-green-500 text-white px-2 py-1 rounded-md">Registrar Cliente</a>
@endsection




